package com.epam.classes;

import com.epam.annotations.MyAnnotation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Person {
    private static final Logger LOGGER = LogManager.getLogger();
    @MyAnnotation(someValue = 4)
    private String name;
    @MyAnnotation
    private int age;
    private String address;

    public Person(String name, int age, String address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }

    boolean util(Person person) {
        LOGGER.info("boolean util(Person person) was invoked!");
        return this.equals(person);
    }

    Person util(String name, int age, String address) {
        LOGGER.info("boolean util(String name, int age, String address) was invoked!");
        return new Person(name, age, address);
    }

    int util(String age) {
        LOGGER.info("boolean util(String age) was invoked!");
        return Integer.parseInt(age);
    }

    void myMethod(String a, int... args) {
        LOGGER.info("void myMethod(" + a + ", " + Arrays.toString(args) + ")");
    }

    void myMethod(String... args) {
        LOGGER.info("void myMethod(" + Arrays.toString(args) + ")");
    }

    @Override
    public String toString() {
        return name + " : " + age + " : " + address;
    }
}
