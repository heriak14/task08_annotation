package com.epam.classes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.stream.Collectors;

public class ObjectAnalyzer {
    private static final Logger LOGGER = LogManager.getLogger();
    private Object object;

    public ObjectAnalyzer(Object object) {
        this.object = object;
    }

    private void printClassInfo() {
        int modifiers = object.getClass().getModifiers();
        LOGGER.info(Modifier.toString(modifiers) + " class " + object.getClass().getSimpleName() + " {");
        Constructor[] constructors = object.getClass().getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            Arrays.stream(constructor.getDeclaredAnnotations()).forEach(a -> LOGGER.info("\t" + a.toString()));
            LOGGER.info("\t" + Modifier.toString(constructor.getModifiers()) + " " + constructor.getName() + "("
                    + Arrays.stream(constructor.getParameterTypes())
                    .map(Class::getSimpleName)
                    .collect(Collectors.joining(", ")) + ");");
        }
    }

    private void printFieldsInfo() {
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            Arrays.stream(field.getDeclaredAnnotations()).forEach(a -> LOGGER.info("\t" + a.toString()));
            LOGGER.info("\t" + Modifier.toString(field.getModifiers()) + " " + field.getName() + ";");
        }
    }

    private void printMethodsInfo() {
        Method[] methods = object.getClass().getDeclaredMethods();
        for (Method method : methods) {
            Arrays.stream(method.getDeclaredAnnotations()).forEach(a -> LOGGER.info("\t" + a.toString()));
            LOGGER.info("\t" + Modifier.toString(method.getModifiers()) + " "
                    + method.getReturnType().getSimpleName() + " " + method.getName() + "("
                    + Arrays.stream(method.getParameterTypes())
                    .map(Class::getSimpleName)
                    .collect(Collectors.joining(", ")) + ");");
        }
    }

    public void getObjectInfo() {
        LOGGER.info("--------Info about object of class " + object.getClass().getSimpleName() + "--------");
        printClassInfo();
        printFieldsInfo();
        printMethodsInfo();
        LOGGER.info("}");
    }
}
