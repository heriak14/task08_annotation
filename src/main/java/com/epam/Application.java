package com.epam;

import com.epam.classes.ObjectAnalyzer;
import com.epam.classes.Person;
import com.epam.classes.ReflectionHandler;

public class Application {
    public static void main(String[] args) {
        ReflectionHandler.printAnnotatedFields();
        ReflectionHandler.invokeMethods();
        ReflectionHandler.setFieldValue();
        ReflectionHandler.invokeMyMethods();
        new ObjectAnalyzer(new Person("Yura", 19, "Lviv")).getObjectInfo();
    }
}
