package com.epam.classes;

import com.epam.annotations.MyAnnotation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ReflectionHandler {

    private static final Logger LOGGER = LogManager.getLogger();
    private static Person john = new Person("John", 21, "Lviv");

    public static void printAnnotatedFields() {
        LOGGER.info("---------Annotated fields of class Person---------");
        Field[] fields = Person.class.getDeclaredFields();
        Arrays.stream(fields)
                .filter(f -> f.isAnnotationPresent(MyAnnotation.class))
                .peek(f -> LOGGER.info(f.getName()))
                .forEach(f -> LOGGER.info("\t@MyAnnotation(someValue = "
                        + f.getAnnotation(MyAnnotation.class).someValue() + ")"));
    }

    public static void invokeMethods() {
        LOGGER.info("--------Invoking 3 methods util() with different parameters-------");
        try {
            Method boolUtil = Person.class.getDeclaredMethod("util", Person.class);
            Method personUtil = Person.class.getDeclaredMethod("util", String.class, int.class, String.class);
            Method intUtil = Person.class.getDeclaredMethod("util", String.class);
            boolUtil.invoke(john, john);
            personUtil.invoke(john, "Sarah", 19, "Kyiv");
            intUtil.invoke(john, "25");
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public static void setFieldValue() {
        LOGGER.info("--------Setting new value for private field--------");
        try {
            Field ageField = Person.class.getDeclaredField("age");
            LOGGER.info("Before setting - " + john);
            ageField.setAccessible(true);
            ageField.set(john, 54);
            LOGGER.info("After setting - " + john);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public static void invokeMyMethods() {
        LOGGER.info("---Invoking myMethod() with different varargs-parameters---");
        try {
            Method myMethod1 = Person.class.getDeclaredMethod("myMethod", String.class, int[].class);
            Method myMethod2 = Person.class.getDeclaredMethod("myMethod", String[].class);
            myMethod1.invoke(john, "abc", new int[]{4, 5, 6, 7});
            myMethod2.invoke(john, (Object) new String[]{"a", "b", "c", "d"});
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
